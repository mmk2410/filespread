# COPYRIGHT 2016 (c) Marcel Kapfer (mmk2410)
# MIT License

gulp = require 'gulp'
coffee = require 'gulp-coffee'
sass = require 'gulp-sass'
dart = require 'gulp-dart'

gulp.task 'coffee', ->
  gulp.src './res/js/*.coffee'
    .pipe coffee()
    .pipe gulp.dest './res/js/'

gulp.task 'sass', ->
  gulp.src './res/css/*.sass'
    .pipe sass()
    .pipe gulp.dest './res/css/'

gulp.task 'dart', ->
  gulp.src './res/dart/*.dart'
    .pipe dart {
      "dest": "./res/dart/",
      "minify": "true",
    }
    .pipe gulp.dest './res/dart/'

gulp.task 'default', ->
  gulp.watch './res/js/*.coffee', ['coffee']
  gulp.watch './res/css/*.sass', ['sass']
  gulp.watch './res/dart/*.dart', ['dart']
