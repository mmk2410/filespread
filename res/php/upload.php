<?php
# COPYRIGHT (c) 2016 Marcel Kapfer (mmk2410)
# MIT License

if (0 < $_FILES['userfile']['error']) {
    echo "Error: " . $_FILES['userfile']['error'] . "<br>";
} else {
    move_uploaded_file($_FILES['userfile']['tmp_name'], "../../assets/file.bin");
    header("Location: ../../");
}
