<?php
# COPYRIGHT (c) 2016 Marcel Kapfer (mmk2410)
# MIT License

$file = "../../templates/" . $_GET["template"];
$content = file($file);

$title = substr($content[0], 7);

$filename = substr($content[1], 10);

$text = substr($content[2], 6);
for ($i = 3; $i < count($content); $i++) {
    $text = $text . $content[$i];
}

echo json_encode(array("subject" => $title, "filename" => $filename, "text" => $text));
