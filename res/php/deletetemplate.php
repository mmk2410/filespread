<?php
# COPYRIGHT (c) 2016 Marcel Kapfer (mmk2410)
# MIT License

$filename = "../../templates/" . $_GET["template"];

if (unlink($filename)) {
    echo 0;
} else {
    echo 1;
}
