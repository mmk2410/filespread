<?php
# COPYRIGHT (c) 2016 Marcel Kapfer (mmk2410)
# MIT License

$file = "../../lists/" . $_GET["list"];
$addresses = file($file);
$res = array();
$i = 0;
foreach ($addresses as $address) {
    $res[] = substr($address, 0, strlen($address) - 1);
    $i++;
}
echo json_encode($res);
