<?php
# COPYRIGHT (c) 2016 Marcel Kapfer (mmk2410)
# MIT License

$filename = "../../lists/" . $_GET["list"];

if (unlink($filename)) {
    echo 0;
} else {
    echo 1;
}
