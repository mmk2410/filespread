<?php
# COPYRIGHT (c) 2016 Marcel Kapfer (mmk2410)
# MIT License

/**
 * configuration map
 */
$ini = parse_ini_file("../../filespread.ini");

date_default_timezone_set($ini["timezone"]);

require '../../vendor/autoload.php';

/**
 * main function that is executed at the beginning.
 *
 * @author Marcel Kapfer <marcelmichaelkapfer@gmail.com>
 * 
 * @param $ini an configuration map.
 */
function main($ini) {

    $data = json_decode($_GET["data"], true);

    $list = "../../lists/" . $data["list"];
    $addresses = file($list);

    $content = $data["body"];

    $title = $data["title"];

    $filename = $data["filename"];

    $nofile = $data["nofile"];

    if ($nofile) {
        $filename = "";
    }

    foreach ($addresses as $address) {
        if (sendMail($title, $content, $filename, $address, $ini) != 0) {
            print "-1";
            exit;
        }
    }

    print "0";

}

/**
 * function for sending the mail.
 *
 * @author Marcel Kapfer <marcelmichaelkapfer@gmail.com>
 * 
 * @param $title the subject of the mail.
 * @param $content the text of the mail.
 * @param $address the receiver of the mail.
 * @param $ini an configuration map.
 */
function sendMail($title, $content, $filename, $address, $ini) {

    $mail = new PHPMailer;
    $mail->CharSet = 'UTF-8';

    $mail->setFrom($ini["email"], $ini["name"]);
    $mail->Subject = $title;
    $mail->Body = $content;

    $mail->addAddress($address);

    if (isset($ini["reply"])) {
        $mail->addReplyTo($ini["reply"]);
    }

    if (!empty($filename)) {
        $mail->AddAttachment("../../assets/file.bin", $filename);
    }
    
    if (!$mail->Send()) {
        return -1;
    } else {
        return 0;
    }
     
}

main($ini);
