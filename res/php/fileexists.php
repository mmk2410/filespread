<?php
# COPYRIGHT (c) 2016 Marcel Kapfer (mmk2410)
# MIT License

$file = "../../" . $_GET["file"];
if (file_exists($file)) {
    echo 0;
} else {
    echo 1;
}
