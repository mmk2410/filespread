<?php
# COPYRIGHT (c) 2016 Marcel Kapfer (mmk2410)
# MIT License

$content = $_GET["data"];

$filename = "../../templates/" . $content["template"];

$file = "Title: " . $content["subject"] . "\n";
$file = $file . "Filename: " . $content["filename"] . "\n";
$file = $file . "Text: " . $content["text"];

$handle = fopen($filename, "w");

if (fwrite($handle, $file)) {
    echo "0";
} else {
    echo "1";
}
