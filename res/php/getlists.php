<?php
/**
 * FileSpread
 * php for json string of lists
 *
 * Copyright (c) 2016 Marcel Kapfer (mmk2410)
 * MIT License
 */

$dir = '../../lists/';
$files = scandir($dir);

$i = 0;
foreach ($files as $file) {
    if ( in_array($file, array(".", "..")) ) {
            unset($files[$i]);
    }
    $i++;
}

echo json_encode(array("lists" => $files));
