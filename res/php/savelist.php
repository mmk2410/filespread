<?php
# COPYRIGHT (c) 2016-2017 Marcel Kapfer (mmk2410)
# MIT License

$content = json_decode($_POST["data"], true);

$filename = "../../lists/" . $content["list"];

$file = "";

foreach ($content["addresses"] as $address) {
    $file = $file . $address . "\n";
}

$handle = fopen($filename, "w");

if (fwrite($handle, $file)) {
    echo "0";
} else {
    echo "1";
}
