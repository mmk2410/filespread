// Copyright (c) 2016 - 2017, Marcel Kapfer (mmk2410).
// MIT License

/// frontend code for filespread, a simple file over email spreading web app.
import 'dart:convert';
import 'dart:html';

/// main method with click listners.
void main() {
  // send mail listener
  querySelector("#send").onClick.listen(sendMail);

  // save address list listner
  querySelector("#save-addresses").onClick.listen(saveAddresses);
}

/// function for calling the backend to send mails.
void sendMail(MouseEvent event) {
  InputElement title = querySelector("#mailtitle");
  InputElement text = querySelector("#mailtext");
  InputElement filename = querySelector("#filename");
  InputElement addresslist = querySelector("#address_list");
  CheckboxInputElement nofile = querySelector("#nofile");

  Map data = {
    "title": title.value,
    "body": text.value,
    "filename": filename.value,
    "list": addresslist.value,
    "nofile": nofile.checked
  };

  String url = "res/php/sendmail.php?data=${JSON.encode(data)}";

  HttpRequest.getString(url).then((String response) {
    if (response == "0") {
      window.alert("Mails successfully sent.");
    } else {
      window.alert("Failed to send mail.");
    }
  });
}

/// function to update the addresses list in the backend.
void saveAddresses(MouseEvent event) {
  TextAreaElement addresses = querySelector("#edit-addresses");
  SelectElement list = querySelector("#edit-address-list");

  List<String> addressesList = new List();

  addresses.value.split("\n").forEach((String address) {
    if (address.isNotEmpty) addressesList.add(address);
  });

  Map data = {"list": list.value, "addresses": addressesList};

  String url = "res/php/savelist.php";

  HttpRequest.postFormData(url, {"data": JSON.encode(data)}).then((HttpRequest response) {
    if (response.responseText == "0") {
      window.alert("Addresses sucessfully saved.");
      updateAddresses(addressesList, list.value);
    } else {
      window.alert("Failed to save adresses.");
    }
  });
}

/// Updated the frontend after an address change.
void updateAddresses(List<String> addresses, String list) {
  SelectElement currentList = querySelector("#address_list");

  if (currentList.value == list) {
    DivElement addressesContainer = querySelector("#addresses");
    addressesContainer.children .removeWhere((Element child) => addressesContainer.children.contains(child));

    addresses.forEach((String address) {
      ParagraphElement addressElement = new ParagraphElement();
      addressElement.text = address;
      addressesContainer.children.add(addressElement);
    });
  }
}
