# CoffeeScript for filespread
# COPYRIGHT (c) 2016 Marcel Kapfer (mmk2410)
# MIT License

main = ->
  updateAddressLists()

  updateTemplateLists()

  $("#address_list").change ->
    updateAddresss $("#address_list").val()

  $("#templates_list").change ->
    updateTemplate $("#templates_list").val()

  $("#edit_templates_list").change ->
    template = $("#edit_templates_list").val()
    if template is "none"
      $(".edit_template").fadeOut()
    else
      updateEditTemplate template

  $("#save_template").click ->
    data =
      template: $("#edit_templates_list").val()
      subject: $("#edit_mailtitle").val()
      filename: $("#edit_filename").val()
      text: $("#edit_mailtext").val()
    $.get "./res/php/savetemplate.php", {"data": data}, (data) ->
      console.log data
      if data is "0"
        alert "Edit sucessfully saved."
        if $("#edit_templates_list").val() is $("#templates_list").val()
          updateTemplate $("#templates_list").val()
      else
        alert "Error while saving."

  $("#create_template").click ->
    filename = $("#create_template_name").val()
    if filename.substring((filename.length  - 4)) isnt ".txt"
      alert "The filename has to end with \".txt\""
    else
      url = "./res/php/fileexists.php?file=templates/#{filename}"
      $.get url, status = (data) ->
        if data is "1"
          data =
            template: filename
            subject: null
            filename: null
            text: null
          $.get "./res/php/savetemplate.php", {"data": data}, (data) ->
            if data is "0"
              alert "Template created sucessfully"
              $("#templates_list").append """
                <option value="#{filename}">#{filename}</option>
              """
              $("#edit_templates_list").append """
                <option value="#{filename}">#{filename}</option>
              """
              $("#remove-template-list").append """
                <option value="#{template}">#{template}</option>
              """
            else
              alert "Error while creating template."
        else
          alert "A template with that filename already exists"

  $("#remove-template-list").change ->
    selected = $("#remove-template-list").val()
    if selected == "none"
      $(".remove_template").fadeOut()
    else
      $(".remove_template").fadeIn()

  $("#remove-template").click ->
    selected = $("#remove-template-list").val()
    url = "./res/php/deletetemplate.php?template=#{selected}"
    $.get url, (data) ->
      if data == "0"
        alert "Template deleted."
        $("#templates_list").empty()
        $("#edit_templates_list").empty()
        $("#remove-template-list").empty()
        $("#edit_templates_list").append """
          <option value="none">None Selected</option>
        """
        $("#remove-template-list").append """
          <option value="none">None Selected</option>
        """
        updateTemplateLists()
        $(".remove_template").fadeOut()
      else
        alert "Failed to delete template."

  $("#edit-address-list").change ->
    selected = $("#edit-address-list").val()
    if selected == "none"
      $(".edit_address").fadeOut()
    else
      url = "./res/php/getaddresses.php?list=#{selected}"
      $.getJSON url, (data) ->
        text = ""
        for address in data
          text = text + address + "\n"
        $("#edit-addresses").val(text)
        $(".edit_address").fadeIn()

  $("#create-list").click ->
    filename = $("#create-list-name").val()
    if filename.substring((filename.length  - 4)) isnt ".txt"
      alert "The filename has to end with \".txt\""
    else
      url = "./res/php/fileexists.php?file=lists/#{filename}"
      $.get url, status = (data) ->
        if data is "1"
          data =
            list: filename
            addresses: [null]
          $.get "./res/php/savelist.php", {"data": data}, (data) ->
            if data is "0"
              alert "List created sucesfully"
              $("#address_list").append """
                <option value="#{filename}">#{filename}</option>
              """
              $("#edit-address-list").append """
                <option value="#{filename}">#{filename}</option>
              """
              $("#remove-list-list").append """
                <option value="#{filename}">#{filename}</option>
              """
            else
              alert "Error while creating template."
        else
          alert "A template with that filename already exists"

  $("#remove-list-list").change ->
    selected = $("#remove-list-list").val()
    if selected == "none"
      $(".remove_list").fadeOut()
    else
      $(".remove_list").fadeIn()

  $("#remove-list").click ->
    selected = $("#remove-list-list").val()
    url = "./res/php/deletelist.php?list=#{selected}"
    $.get url, (data) ->
      if data == "0"
        alert "List deleted."
        $("#address_list").empty()
        $("#edit-address-list").empty()
        $("#remove-list-list").empty()
        $("#edit-address-list").append """
          <option value="none">None Selected</option>
        """
        $("#remove-list-list").append """
          <option value="none">None Selected</option>
        """
        updateAddressLists()
        updateAddresss $("#address_list").val()
        $(".remove_list").fadeOut()
      else
        alert "Failed to delete template."

  $("#edit-address-list").change ->
    selected = $("#edit-address-list").val()
    if selected == "none"
      $(".edit_address").fadeOut()
    else
      url = "./res/php/getaddresses.php?list=#{selected}"
      $.getJSON url, (data) ->
        text = ""
        for address in data
          text = text + address + "\n"
        $("#edit-addresses").val(text)
        $(".edit_address").fadeIn()


$(document).ready main

updateTemplateLists = ->
  $.getJSON './res/php/gettemplates.php', (data) ->
    for templates, template of data.templates
      $("#templates_list").append """
        <option value="#{template}">#{template}</option>
      """
      $("#edit_templates_list").append """
        <option value="#{template}">#{template}</option>
      """
      $("#remove-template-list").append """
        <option value="#{template}">#{template}</option>
      """
    updateTemplate data.templates[2]

updateAddresss = (list) ->
  url = './res/php/getaddresses.php?list=' + list
  $.getJSON url, (data) ->
    $("#addresses").empty()
    for address in data
      $("#addresses").append "<p>#{address}</p>"

updateTemplate = (template) ->
  url = './res/php/gettemplate.php?template=' + template
  $.getJSON url, (data) ->
    $("#mailtitle").val data.subject
    $("#filename").val data.filename
    $("#mailtext").val data.text

updateEditTemplate = (template) ->
  url = './res/php/gettemplate.php?template=' + template
  $.getJSON url, (data) ->
    $(".edit_template").fadeIn()
    $("#edit_mailtitle").val data.subject
    $("#edit_filename").val data.filename
    $("#edit_mailtext").val data.text

updateAddressLists = ->
  $.getJSON './res/php/getlists.php', (data) ->
    for lists, list of data.lists
      $("#address_list").append """
        <option value="#{list}">#{list}</option>
      """
      $("#edit-address-list").append """
        <option value="#{list}">#{list}</option>
      """
      $("#remove-list-list").append """
        <option value="#{list}">#{list}</option>
      """
    updateAddresss data.lists[2]
