# Filespread

Filespread is a simple webapp to make file spreading over mail easy by providing the ability to manage different templates and different address lists.

## Installation

Fork the git repo and run

```
npm install
```

```
composer install
```

Make sure you have gulp and the Dart SDK installed on your system and run

```
gulp sass
```

```
gulp coffee
```

```
gulp dart
```

Create the folders `templates`, `assets` and `lists` and make sure that the server can write in these folders.

Finally remove the example files.

# License

Filespread is licensed under the MIT license.

# Contributing

1. Fork it
2. Create a feature branch with a meaningful name (`git checkout -b my-new-feature`)
3. Add yourself to the CONTRIBUTORS file
4. Commit your changes (`git commit -am 'Add some feature'`)
5. Push to your branch (`git push origin my-new-feature`)
6. Create a new pull request
